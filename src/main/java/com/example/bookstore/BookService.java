package com.example.bookstore;

import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Stream;

@Service
public class BookService {

    public Stream<BookEntity> all() {
        return Stream.of(
                new BookEntity(UUID.randomUUID().toString(), "Harry Potter", "J.K. Rowling"),
                new BookEntity(UUID.randomUUID().toString(), "Sagan Om Ringen", "J.R.R Tolkien"));
    }

    public BookEntity createBook(String title, String author) {
        return new BookEntity(
                UUID.randomUUID().toString(),
                title,
                author);
    }

    public BookEntity get(String id) {
        return new BookEntity(
                id,
                "Harry Potter",
                "J.K. Rowling");
    }

/*    public BookEntity updateBook() {

    }*/
}
