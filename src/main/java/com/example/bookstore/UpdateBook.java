package com.example.bookstore;

import lombok.Value;

@Value
public class UpdateBook {

    String title;
    String author;

}
