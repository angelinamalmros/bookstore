package com.example.bookstore;

import lombok.Value;

@Value
public class CreateBook {

    String title;
    String author;

}
