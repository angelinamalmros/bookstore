package com.example.bookstore;

import lombok.Value;

@Value
public class Book {

    String id;
    String title;
    String author;

}
