package com.example.bookstore;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping("/books")
public class BookController {

    BookService bookService;

    @GetMapping()
    public List<Book> all() {
        return bookService.all()
                .map(BookController::toDTO)
                .collect(Collectors.toList());
    }

    @PostMapping
    public Book createBook(@RequestBody CreateBook createBook) {
        return toDTO(
                bookService.createBook(
                        createBook.getTitle(),
                        createBook.getAuthor()));
    }

    @GetMapping("/{id}")
    public Book getBookById(@PathVariable("id") String id) {
        return toDTO(bookService.get(id));
    }

/*    @PutMapping("/{id}")
    public Book updateBook(@PathVariable("id") String id, @RequestBody UpdateBook updateBook) {
        return toDTO(
                bookService.updateBook(
                        updateBook.getTitle(),
                        updateBook.getAuthor()));
    }*/

    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable("id") String id) {
    }

    private static Book toDTO(BookEntity bookEntity) {
        return new Book(
                bookEntity.getId(),
                bookEntity.getTitle(),
                bookEntity.getAuthor());
    }


}
