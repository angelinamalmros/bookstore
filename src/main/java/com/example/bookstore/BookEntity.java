package com.example.bookstore;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookEntity {

    String id;
    String title;
    String author;

}
