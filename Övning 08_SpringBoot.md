# Lektion 8 - Avancerad Java

## Övning - Spring Boot

## Översikt
Uppgiften behandlar Spring Boot - REST
Uppgiften går ut på att skapa en förteckning av böcker som vi kan jobba med på olika sätt. 
Vi ska till exempel kunna lista alla böcker, skapa (lägga till) en bok, 

## Genomförande
Denna uppgift är inte ett obligatoriskt moment i kursen och genomförs individuellt eller i grupp. 

### Övning 1
1. Skapa ett Spring Boot projekt - använd Spring Initializer och välj Spring Web samt Lombok

### Övning 2
1. Skapa ett projekt på GitLab
2. Projektet ska vara publikt
3. Projektet ska inte ha någon README fil
4. Koppla ihop ditt IntelliJ projekt med GitLab - gör en commit/push och se till att alla filer kommer med
5. Kolla även .gitignore filen så att IntelliJ's arbetsfiler inte kommer med. Även target filerna ska ligga i gitignore

### Övning 3
1. Skapa en java klass med namnet Book och ge den ett fält "name" (String)
2. Annotera klassen med @Value (från Lombok)
3. Skapa en till java klass och ge den namnet BookController, annotera klassen med @RestController
4. Annotera BookController med @RequestMapping("/books")
5. Skapa en metod med namnet "all" i klasse BookController, som returnerar en lista med Book
6. Annotera metoden med @GetMapping
7. Skapa en lista med ett par hårdkodade böcker och returnera din lista med böcker i metoden "all" (return List.of(new Book(name...)))
8. Testa så att din lista skrivs ut i webbläsaren
9. http://localhost:8080/books

### Övning 4 - Skapa/Lägg till
1. Skapa en klass som heter CreateBook och annotera den med @Value
2. Lägg till följande fält till CreateBook: Name samt Author (Lägg till Author även i Book-klassen)
3. Lägg till en metod createBook i klassen BookController, denna metod ska returnera Book
4. Annotera metoden med @PostMapping. Metoden ska ta in en parametern CreateBook
5. Annotera parametern "CreateBook" med @RequestBody (@RequestBody CreateBook createBook)
6. Metoden ska returnera en ny bok - new Book

### Övning 5 - Hämta en bok med ett specifikt id
1. Lägg till String id som ett fält i klassen Book. 
2. Använd UUID.randomUUID().toString() för att skapa ett id, används då man skapar ett objekt av klassen Book. (java.util.UUID)
3. Lägg till en metod i BookController med namnet getBookById
4. Metoden ska returnera Book
5. Annotera metoden med @GetMapping("/{id}")
6. Metoden ska ta in parametern String id och annoteras med @PathVariable (@PathVariable("id") String id)
7. I metodens body ska ett nytt Book objekt returneras

### Övning 6 - Uppdatera en bok
1. Skapa en klass som heter UpdateBook
2. Annotera klassen med @Value
3. Klassen ska ha följande fält - Name och Author 
4. Lägg till en metod i klassen BookController med namnet updateBook
5. Metoden ska returnera Book
6. Annotera metoden med @PutMapping("/{id}")
7. Metoden ska ta in parameterna UpdateBook och String id, id ska annoteras med @PathVariable och UpdateBook ska annoteras med @RequestBody (@PathVariable("id") String id, @RequestBody UpdateBook updateBook)
8. I metodens body ska ett nytt Book objekt returneras

### Övning 7 - Radera en bok
1. Lägg till en metod i BookController med namnet deleteBook
2. Metoden ska inte returnera något - void
3. Annotera metoden med @DeleteMapping("/{id}")
4. Metoden ska ta in String id som parameter, annotera parametern med @PathVariable("id")
